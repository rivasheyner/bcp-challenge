import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailAgencyComponent } from './containers/detail-agency/detail-agency.component';
import { ListAgenciesComponent } from './containers/list-agencies/list-agencies.component';
import { DefaultComponent } from './core/layout/default/default.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'agencias',
    pathMatch: 'full',
  },
  {
    path: 'agencias',
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: ListAgenciesComponent,
      },
      {
        path: 'detalle',
        component: DetailAgencyComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
