import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs';
import { AgencyInterface } from 'src/app/interfaces/interfaces';
import { AgencyMaintenanceService } from 'src/app/services/agency-maintenance/agency-maintenance.service';
import { FormAgencyActionService } from 'src/app/services/form-agency-action/form-agency-action.service';
import { AgenciesDataState } from 'src/app/states/agency-data/agencies-data.state';
import { FormAction } from '../containers.config';

@Component({
  selector: 'app-list-agencies',
  templateUrl: './list-agencies.component.html',
  styleUrls: ['./list-agencies.component.scss'],
})
export class ListAgenciesComponent implements OnInit {
  agencies = [];

  constructor(
    private router: Router,
    private agencyMaintenanceService: AgencyMaintenanceService,
    private agenciesDataState: AgenciesDataState,
    private formAgencyActionService: FormAgencyActionService
  ) {}

  ngOnInit(): void {
    this.agenciesDataState
      .onDataAgencies$()
      .pipe(tap((agencies) => (this.agencies = agencies)))
      .subscribe();
  }

  onShowDetail(agency: AgencyInterface, id: any) {
    this.formAgencyActionService.setActionFormStatus(FormAction.Update);
    this.agencyMaintenanceService.setFormStatus({ ...agency, id: id });
    this.router.navigate(['agencias/detalle']);
  }

  onAdd() {
    this.formAgencyActionService.setActionFormStatus(FormAction.Add);
    this.agencyMaintenanceService.setFormStatus('');
    this.router.navigate(['agencias/detalle']);
  }
}
