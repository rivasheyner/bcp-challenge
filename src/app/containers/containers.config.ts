export const enum FormAction {
  Add = 'ADD',
  Update = 'UPDATE',
  Default = 'DEFAULT',
}
