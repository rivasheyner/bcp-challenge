import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../components/components.module';
import { ListAgenciesComponent } from './list-agencies/list-agencies.component';
import { DetailAgencyComponent } from './detail-agency/detail-agency.component';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ListAgenciesComponent, DetailAgencyComponent],
  imports: [CommonModule, ComponentsModule, MatInputModule, MatButtonModule, FormsModule, ReactiveFormsModule, ],
  exports: [ListAgenciesComponent, DetailAgencyComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ContainersModule {}
