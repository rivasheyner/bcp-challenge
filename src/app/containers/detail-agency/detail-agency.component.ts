import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { map, merge, tap } from 'rxjs';
import { AgencyInterface } from 'src/app/interfaces/interfaces';
import { AgenciesDataService } from 'src/app/services/agencies-data/agencies-data.service';
import { AgencyMaintenanceService } from 'src/app/services/agency-maintenance/agency-maintenance.service';
import { FormAgencyActionService } from 'src/app/services/form-agency-action/form-agency-action.service';
import { FormAction } from '../containers.config';
import { agencyForm } from './detail-agency.form';

@Component({
  selector: 'app-detail-agency',
  templateUrl: './detail-agency.component.html',
  styleUrls: ['./detail-agency.component.scss'],
})
export class DetailAgencyComponent implements OnInit, OnDestroy {
  agency: AgencyInterface = {
    agencia: '',
    direccion: '',
    distrito: '',
    lat: '',
    lon: '',
  };
  update = false;
  form: FormGroup = agencyForm();
  mapSrc: SafeUrl = '';

  constructor(
    private router: Router,
    private agencyMaintenanceService: AgencyMaintenanceService,
    private agenciesDataService: AgenciesDataService,
    private sanitizer: DomSanitizer,
    private formAgencyActionService: FormAgencyActionService
  ) {}
  ngOnDestroy(): void {
    this.agencyMaintenanceService.setFormStatus('');
  }

  ngOnInit(): void {
    this.agencyMaintenanceService
      .onFormStatus$()
      .pipe(
        tap((agency) => (this.agency = agency)),
        tap((agency) => this.form.patchValue(agency)),
        tap((agency) => (this.update = !!agency)),
        tap(
          (agency) =>
            (this.mapSrc = this.sanitizer.bypassSecurityTrustResourceUrl(
              `https://www.google.com/maps/embed/v1/place?q=${
                this.form.get('lat')?.value
              },${
                this.form.get('lon')?.value
              }&key=AIzaSyDylTy_XHxyK711MhK0Rvd20xtR49Jd5R4&q=Space+Needle,Seattle+WA`
            ))
        )
      )
      .subscribe();
  }

  onClick() {
    if (this.update) {
      this.agenciesDataService.updateDataStatus(this.form.value);
    } else {
      this.agenciesDataService.setDataStatus(this.form.value);
    }
    this.formAgencyActionService.setActionFormStatus(FormAction.Default);
    this.router.navigate(['/']);
  }
  onCancel() {
    this.formAgencyActionService.setActionFormStatus(FormAction.Default);
    this.router.navigate(['/']);
  }
}
