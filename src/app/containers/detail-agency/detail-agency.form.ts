import { FormControl, FormGroup, Validators } from '@angular/forms';

export const agencyForm = () =>
  new FormGroup({
    id: new FormControl(''),
    agencia: new FormControl('', [Validators.required]),
    direccion: new FormControl('', [Validators.required]),
    distrito: new FormControl('', [Validators.required]),
    lat: new FormControl('', [Validators.required]),
    lon: new FormControl('', [Validators.required]),
  });
