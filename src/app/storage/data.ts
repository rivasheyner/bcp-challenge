export const agencies = [
  {
    agencia: 'Las Flores',
    distrito: 'San Juan De Lurigancho',
    provincia: 'Lima',
    departamento: 'Lima',
    direccion: 'Las Flores de Primavera 1487',
    lat: -12.0449737,
    lon: -77.0485157,
    img: 'agencia-1.jpeg'
  },

  {
    agencia: 'Punchana',
    distrito: 'Punchana',
    provincia: 'Maynas',
    departamento: 'Loreto',
    direccion: 'Av. La Marina N° 944',
    lat: -12.0442737,
    lon: -77.0385157,
    img: 'agencia-2.jpeg'
  },
  {
    agencia: 'Conquistadores',
    distrito: 'San Isidro',
    provincia: 'Lima',
    departamento: 'Lima',
    direccion: 'Av. Conquistadores 968',
    lat: -12.0483537,
    lon: -77.0339337,
    img: 'agencia-3.jpeg'
  },
  {
    agencia: 'Salvador Allende',
    distrito: 'Villa Maria del Triunfo',
    provincia: 'Lima',
    departamento: 'Lima',
    direccion: 'Av. Salvador Allende 468 – Villa María del Triunfo',
    lat: -76.957646,
    lon: -12.158153,
    img: 'agencia-4.jpeg'
  },
  {
    agencia: 'Siglo XX (030)',
    distrito: 'Arequipa',
    provincia: 'Arequipa',
    departamento: 'Arequipa',
    direccion: 'Av. Siglo XX N°122 Cercado de',
    lat: -12.1473927,
    lon: -76.9702412,
    img: 'agencia-5.jpeg'
  },
];
