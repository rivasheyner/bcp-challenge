import {
  AfterViewChecked,
  Component,
  OnInit,
} from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs';
import { LoaderStatusService } from 'src/app/services/loader/loader-status.service';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss'],
})
export class DefaultComponent implements OnInit, AfterViewChecked {
  constructor(
    private router: Router,
    public loaderStatusService: LoaderStatusService
  ) {
    this.loaderStatusService.isLoading = true;
  }

  ngAfterViewChecked(): void {
    setTimeout(() => {
      this.loaderStatusService.isLoading = false;
    }, 1000);
  }

  ngOnInit(): void {
    this.router.events
      .pipe(
        filter((event) => {
          if (event instanceof NavigationStart) {
            this.loaderStatusService.isLoading = true;
            return event instanceof NavigationEnd;
          }
          if (event instanceof NavigationEnd) {
            this.loaderStatusService.isLoading = false;
            return event instanceof NavigationEnd;
          }
          return false;
        })
      )
      .subscribe();
  }
}
