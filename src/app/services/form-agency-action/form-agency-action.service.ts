import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FormAgencyActionState } from 'src/app/states/form-agency-action/form-agency-action.state';

@Injectable({
  providedIn: 'root',
})
export class FormAgencyActionService {
  constructor(private status: FormAgencyActionState) {}

  onActionFormStatus$(): Observable<any> {
    return this.status.onActionFormStatus$();
  }

  setActionFormStatus(status: any): void {
    return this.status.setActionFormStatus(status);
  }
}
