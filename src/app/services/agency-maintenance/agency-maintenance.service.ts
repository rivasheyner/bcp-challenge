import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AgencyMaintenanceState } from 'src/app/states/agency-maintenance/agency-maintenance.state';

@Injectable({
  providedIn: 'root',
})
export class AgencyMaintenanceService {
  constructor(private status: AgencyMaintenanceState) {}

  onFormStatus$(): Observable<any> {
    return this.status.onFormStatus$();
  }

  setFormStatus(status: any): void {
    return this.status.setFormStatus(status);
  }
}
