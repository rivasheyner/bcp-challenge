import { TestBed } from '@angular/core/testing';

import { AgencyMaintenanceService } from './agency-maintenance.service';

describe('AgencyMaintenanceService', () => {
  let service: AgencyMaintenanceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgencyMaintenanceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
