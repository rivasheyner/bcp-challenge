import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoaderStatusService {
  isLoading = false;
  constructor() {}
}
