import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AgenciesDataState } from 'src/app/states/agency-data/agencies-data.state';

@Injectable({
  providedIn: 'root',
})
export class AgenciesDataService {
  constructor(private dataState: AgenciesDataState) {}

  onDataStatus$(): Observable<any> {
    return this.dataState.onDataAgencies$();
  }

  setDataStatus(status: any): void {
    return this.dataState.setDataAgencies(status);
  }

  setSearchResult(agencies: any): void {
    return this.dataState.setSearchResult(agencies);
  }

  updateDataStatus(status: any): void {
    return this.dataState.updateDataAgency(status);
  }
}
