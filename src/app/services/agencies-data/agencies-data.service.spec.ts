import { TestBed } from '@angular/core/testing';

import { AgenciesDataService } from './agencies-data.service';

describe('AgenciesDataService', () => {
  let service: AgenciesDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgenciesDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
