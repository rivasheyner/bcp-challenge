import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-agency',
  templateUrl: './card-agency.component.html',
  styleUrls: ['./card-agency.component.scss'],
})
export class CardAgencyComponent implements OnInit {
  @Input() agency: any = {};
  constructor() {}

  ngOnInit(): void {}
}
