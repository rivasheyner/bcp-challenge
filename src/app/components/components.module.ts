import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardAgencyComponent } from './card-agency/card-agency.component';
import { HeaderComponent } from './header/header.component';
import { LoaderComponent } from './loader/loader.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import { AddComponent } from './add/add.component';
import { RouterModule } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CardAgencyComponent, HeaderComponent, LoaderComponent, AddComponent],
  imports: [CommonModule, MatToolbarModule, MatIconModule, MatCardModule, RouterModule, MatInputModule, FormsModule],
  exports: [CardAgencyComponent, HeaderComponent, LoaderComponent, AddComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule {}
