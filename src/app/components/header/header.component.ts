import { Component, OnInit } from '@angular/core';
import { map, tap } from 'rxjs';
import { FormAction } from 'src/app/containers/containers.config';
import { AgenciesDataService } from 'src/app/services/agencies-data/agencies-data.service';
import { AgencyMaintenanceService } from 'src/app/services/agency-maintenance/agency-maintenance.service';
import { FormAgencyActionService } from 'src/app/services/form-agency-action/form-agency-action.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  showInputSearch = false;
  isInForm = false;
  agencySearch = '';
  agenciesSaved = [];
  pureData = [];
  action: any = FormAction.Default;
  defaultAction = FormAction.Default;
  updateAction = FormAction.Update;
  addAction = FormAction.Add;

  constructor(
    private agencyMaintenanceService: AgencyMaintenanceService,
    private agenciesDataService: AgenciesDataService,
    private formAgencyActionService: FormAgencyActionService
  ) {}

  ngOnInit(): void {
    this.formAgencyActionService
      .onActionFormStatus$()
      .pipe(
        map((action) => (this.action = action)),
        map(() => this.search(''))
      )
      .subscribe();

    this.agencyMaintenanceService
      .onFormStatus$()
      .pipe(tap((data) => (this.isInForm = !!data)))
      .subscribe();
  }

  onClickSearch() {
    this.showInputSearch = !this.showInputSearch;
  }

  search(valueSearching: any) {
    let expresion = new RegExp(valueSearching.toUpperCase(), 'g');
    const agenciesPersisted = JSON.parse(
      localStorage.getItem('agencies') || ''
    );
    let got = agenciesPersisted.filter((agency: any) =>
      expresion.test(agency.agencia.toUpperCase())
    );

    this.agenciesDataService.setSearchResult(got);
  }
}
