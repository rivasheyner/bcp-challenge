import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, pipe, tap } from 'rxjs';
import { agencies } from 'src/app/storage/data';

@Injectable({
  providedIn: 'root',
})
export class AgenciesDataState {
  dataForm = '';
  private state$ = new BehaviorSubject<any>([]);

  constructor() {
    // Just for simulate persistence
    if (!JSON.parse(localStorage.getItem('agencies')!)) {
      localStorage.setItem('agencies', JSON.stringify(agencies));
      this.state$.next(agencies);
    } else {
      this.state$.next(JSON.parse(localStorage.getItem('agencies')!));
    }
  }

  onDataAgencies$(): Observable<any> {
    return this.state$.asObservable();
  }

  setDataAgencies(agency: any) {
    const agencies = [...this.state$.value, agency];
    localStorage.setItem('agencies', JSON.stringify(agencies));
    this.state$.next(agencies);
  }

  setSearchResult(agencies: any) {
    this.state$.next(agencies);
  }

  updateDataAgency(agencyToUpdate: any) {
    this.state$.value[agencyToUpdate.id] = agencyToUpdate;
    localStorage.setItem('agencies', JSON.stringify(this.state$.value));
    this.state$.next(this.state$.value);
  }
}
