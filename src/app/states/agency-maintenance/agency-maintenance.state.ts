import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AgencyMaintenanceState {
  constructor() {}

  private state$ = new BehaviorSubject<any>('');

  onFormStatus$(): Observable<any> {
    return this.state$.asObservable();
  }

  setFormStatus(state: any): void {    
    this.state$.next(state);
  }
}
