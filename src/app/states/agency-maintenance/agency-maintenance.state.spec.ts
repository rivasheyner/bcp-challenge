import { TestBed } from '@angular/core/testing';

import { AgencyMaintenance } from './agency-maintenance.state';

describe('AgencyMaintenance.TstateService', () => {
  let service: AgencyMaintenance;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgencyMaintenance);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
