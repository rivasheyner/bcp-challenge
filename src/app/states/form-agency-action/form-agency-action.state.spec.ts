import { TestBed } from '@angular/core/testing';

import { FormAgencyActionService } from './form-agency-action.state';

describe('FormAgencyActionService', () => {
  let service: FormAgencyActionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormAgencyActionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
