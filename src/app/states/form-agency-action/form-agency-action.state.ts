import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormAction } from 'src/app/containers/containers.config';

@Injectable({
  providedIn: 'root',
})
export class FormAgencyActionState{
  constructor() {}

  private state$ = new BehaviorSubject<any>(FormAction.Default);

  onActionFormStatus$(): Observable<any> {
    return this.state$.asObservable();
  }

  setActionFormStatus(state: any): void {
    this.state$.next(state);
  }
}
