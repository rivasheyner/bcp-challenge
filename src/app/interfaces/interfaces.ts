export interface AgencyInterface {
  agencia: string;
  direccion: string;
  distrito: string;
  lat: string;
  lon: string;
}